/*
Author: Benoit d'Aramon
Date: 23/09/2017
Comment: Arduino code for the timer for the Polaroid Fujifilm Instax 210
*/

void setup() {
  pinMode(0, OUTPUT); //octocupler
  pinMode(1, OUTPUT); //LED
  pinMode(2, INPUT); //push button
}
int interruptor=0;

void loop() {
  interruptor=digitalRead(2);
  if (interruptor==HIGH){
    blink(1);      
    delay(800);   
    
    blink(1); 
    blink(1); 
    delay(1200);
    
    blink(1); 
    blink(1); 
    blink(1); 
    delay(900);

    blink(1); 
    blink(1);
    blink(1); 
    blink(1);
    delay(400);

    blink(1); 
    blink(1);
    blink(1); 
    blink(1);
    blink(1);
    
    delay(200); 
    digitalWrite(0, HIGH); 
    delay(1000);             
    digitalWrite(0, LOW); 
    delay(100); 
  }

}

void blink(int pin){
   digitalWrite(pin, HIGH); 
    delay(100);             
    digitalWrite(pin, LOW);  
    delay(100); 
  
}

